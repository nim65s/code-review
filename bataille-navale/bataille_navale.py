# -*- coding: utf-8 -*-

from random import randint

import numpy as np

import grille_ordi as go
import remplissage_grille as rg

print "Règles du jeu:"
print "Chaque joueur dispose d'une grille 10x10 (les cases sont numérotées de 0 à 9) sur laquelle il doit placer 10 bateaux."
print "Chacun votre tour, vous allez tirer sur une case de la grille adverse."
print "Si vous touchez un bateau, il sera affiché 'Touché!', sinon il sera affiché 'A l'eau'."
print "Le but du jeu est de faire couler tous les bateaux de l'adversaire."
print "Attention: Les bateaux doivent rentrer dans la grille et ne doivent pas se chevaucher. Ils doivent être placés sur une seule ligne ou colonne (pas en diagonale!).\n"

L = (10, 10)
# A est la grille du joueur
A = np.zeros(L)
# B est la grille du joueur
B = np.zeros(L)
# Pour le moment, les grilles sont vides, il faut y placer les bateaux

print "Pour commencer, placez vos bateaux."
print "Vous donnerez chaque case sous la forme [x,y]."
# On demande au joueur de placer chaque bateau

porte_avion1 = input('Donnez une case pour placer votre porte-avion de 5 cases:')
porte_avion2 = input('Donnez une seconde case pour placer votre porte-avion distante de 4 cases de la précédente:')
rg.remplissage_grille(A, porte_avion1, porte_avion2, 1)
print "Votre porte-avion est désigné par des 1."

croiseur1 = input('Donnez une case pour placer votre croiseur de 4 cases:')
croiseur2 = input('Donnez une seconde case pour placer votre croiseur distante de 3 cases de la précédente:')
rg.remplissage_grille(A, croiseur1, croiseur2, 2)
print "Votre croiseur est désigné par des 2."

contre_torpilleur1 = input('Donnez une case pour placer votre contre torpilleur de 3 cases:')
contre_torpilleur2 = input(
    'Donnez une seconde case pour placer votre contre torpilleur distante de 2 cases de la précédente:')
rg.remplissage_grille(A, contre_torpilleur1, contre_torpilleur2, 3)
print "Votre contre-torpilleur est désigné par des 3."

sous_marin1 = input('Donnez une case pour placer votre sous-marin de 3 cases:')
sous_marin2 = input('Donnez une seconde case pour placer votre sous-marin distante de 4 cases de la précédente:')
rg.remplissage_grille(A, sous_marin1, sous_marin2, 4)
print "Votre sous-marin est désigné par des 4."

torpilleur1 = input('Donnez une case pour placer votre torpilleur de 2 cases:')
torpilleur2 = input('Donnez une seconde case pour placer votre torpilleur distante de 1 case de la précédente:')
rg.remplissage_grille(A, torpilleur1, torpilleur2, 5)
print "Votre torpilleur est désigné par des 5."

print "Une case touchée est désignée par un 8."

# On remplit aléatoirement la grille de l'ordinateur
B = go.placer_bateaux_ordi(B)

print "On peut commencer à jouer! \n"
# Début du jeu

while not rg.gagne(A) and not rg.gagne(B):
    # Le joueur joue
    print "A vous de jouer:"
    [x1, y1] = input("Tirez sur une case:")

    if B[x1, y1] == 0:  # Le joueur tire dans l'eau
        B[x1, y1] = 8
        print "A l'eau \n"

    elif B[x1, y1] == 8:  # Le joueur tire sur une case déjà touchée
        print "Vous aviez déjà tiré sur cette case: dommage! \n"

    else:  # Le joueur tire sur un bateau
        v = B[x1, y1]
        B[x1, y1] = 8
        t = sum(sum(B == v))
        if t != 0:  # Il reste des cases contenant ce bateau
            print "Touché! \n"
        else:  # Toutes les cases contenant ce bateau ont été touchées: le bateau est coulé
            print "Touché! Coulé!"
            if v == 1:
                print "Vous avez coulé le porte-avion \n"
            if v == 2:
                print "Vous avez coulé le croiseur \n"
            if v == 3:
                print "Vous avez coulé le contre-torpilleur \n"
            if v == 4:
                print "Vous avez coulé le sous-marin \n"
            if v == 5:
                print "Vous avez coule le torpilleur \n"

    if not rg.gagne(B):  # L'ordinateur ne joue que si le joueur n'a pas gagné à ce tour
        print "L'ordinateur joue:"

        # L'ordinateur tire aléatoirement sur une case sur laquelle il n'a pas déjà tiré
        x2 = randint(0, 9)
        y2 = randint(0, 9)

        while A[x2, y2] == 8:  # Si l'ordinateur donne une case qu'il a déjà touché, il donne une nouvelle case
            x2 = randint(0, 9)
            y2 = randint(0, 9)

        if A[x2, y2] == 0:
            A[x2, y2] = 8
            print "A l'eau"

        else:
            t = 0
            v = A[x2, y2]
            A[x2, y2] = 8
            t = sum(sum(A == v))
            if t != 0:
                print "Touché!"
            else:
                print "Touché! Coulé! "
                if v == 1:
                    print "Votre porte-avion a été coulé"
                if v == 2:
                    print "Votre croiseur a été coulé"
                if v == 3:
                    print "Votre contre-torpilleur a été coulé"
                if v == 4:
                    print "Votre sous-marin a été coulé"
                if v == 5:
                    print "Votre torpilleur a été coulé"
        print A, "\n"  # On montre au joueur l'état de sa grille

# Le jeu s'arrête lorsque quelqu'un gagne

if rg.gagne(A):
    print "Vous avez perdu :("
if rg.gagne(B):
    print "Felicitations! Vous avez gagné! :D"
