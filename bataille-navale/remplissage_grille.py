# -*- coding: utf-8 -*-

import numpy as np


def remplissage_grille(A, X, Y, n):
    """ A est la grille à remplir, X est la première case, Y la deuxième case, n le chiffre rempli dans la grille """
    x1 = X[0]
    y1 = X[1]

    x2 = Y[0]
    y2 = Y[1]

    while x1 != x2 and y1 != y2:
        print "/!\ /!\ Attention, les bateaux ne peuvent pas être en diagonale!"
        X = input('Redonnez la première case du bateau:')
        Y = input('Redonnez la dernière case du bateau:')
        x1 = X[0]
        y1 = X[1]
        x2 = Y[0]
        y2 = Y[1]

    a = 0
    if x1 == x2:
        for i in range(y1, y2 + 1):
            a += A[x1, i]
    if y1 == y2:
        for i in range(x1, x2 + 1):
            a += A[i, y1]

    while a != 0:
        print "/!\ /!\ Attention,vos bateaux se chevauchent!"
        X = input('Redonnez la première case du bateau:')
        Y = input('Redonnez la dernière case du bateau:')
        x1 = X[0]
        y1 = X[1]
        x2 = Y[0]
        y2 = Y[1]
        while x1 != x2 and y1 != y2:
            print "/!\ /!\ Attention, les bateaux ne peuvent pas être en diagonale!"
            X = input('Redonnez la première case du bateau:')
            Y = input('Redonnez la dernière case du bateau:')
            x1 = X[0]
            y1 = X[1]
            x2 = Y[0]
            y2 = Y[1]
        a = 0
        if x1 == x2:
            for i in range(y1, y2 + 1):
                a += A[x1, i]
        if y1 == y2:
            for i in range(x1, x2 + 1):
                a += A[i, y1]

    if x1 == x2:
        y1, y2 = sorted((y1, y2))
        A[x1, y1:y2 + 1] = n

    if y1 == y2:
        x1, x2 = sorted((x1, x2))
        A[x1:x2 + 1, y1] = n

    print(A)
    return None


def gagne(A):
    """ On vérifie si un joueur gagne (plus aucun bateau sur la grille adverse) """
    return sum(sum((A == 0) + (A == 8))) == 100


# Test remplissage_grille
if __name__ == '__main__':
    print "Test de remplissage_grille:"

    A = np.array([[0, 0], [0, 0]])
    B = np.array([[0, 0], [0, 0]])
    X = [0, 0]
    Y = [0, 1]
    remplissage_grille(A, X, Y, 1)
    if A[0, 0] == 1 and A[0, 1] == 1 and A[1, 0] == 0 and A[1, 1] == 0:
        test = "OK"
    else:
        test = "NOK"
    print "remplissage_grille({})={} ; Résultat attendu ={}".format((B, X, Y, 1), A, np.array([[1, 1], [0, 0]]))
    print " ---> test {} \n".format(test)

    A = np.array([[0, 0], [0, 0]])
    X = [1, 0]
    Y = [0, 0]
    remplissage_grille(A, X, Y, 2)
    if A[0, 0] == 2 and A[0, 1] == 0 and A[1, 0] == 2 and A[1, 1] == 0:
        test = "OK"
    else:
        test = "NOK"
    print "remplissage_grille({})={} ; Résultat attendu ={}".format((B, X, Y, 1), A, np.array([[2, 0], [2, 0]]))
    print " ---> test {} \n".format(test)

# Test gagne
if __name__ == '__main__':
    print "Test de gagne:"

    A = np.array([[1, 1, 1, 1, 1, 0, 0, 0, 0, 0], [0, 0, 2, 0, 0, 0, 0, 0, 0, 0], [0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 2, 0, 0, 0, 0, 5, 0, 0], [0, 0, 2, 0, 0, 0, 0, 5, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 4, 4, 4, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 3, 3, 3, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])

    if not gagne(A):
        test = "OK"
    else:
        test = "NOK"
    print "gagne({})={} ; Résultat attendu ={}".format(A, gagne(A), False)
    print " ---> test {} \n".format(test)

    B = np.array([[8, 8, 8, 8, 8, 0, 0, 0, 0, 0], [0, 0, 8, 0, 0, 0, 0, 0, 0, 0], [0, 0, 8, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 8, 0, 0, 0, 0, 8, 0, 0], [0, 0, 8, 0, 0, 0, 0, 8, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 8, 8, 8, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 8, 8, 8, 0, 0, 0],
                  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
    if gagne(B):
        test = "OK"
    else:
        test = "NOK"
    print "gagne({})={} ; Résultat attendu ={}".format(B, gagne(B), False)
    print " ---> test {} \n".format(test)
