# -*- coding: utf-8 -*-

import copy
from random import randint

import numpy as np

# On crée des couples du type (numéro du bateau, taille du bateau)
bateaux = [(1, 5), (2, 4), (3, 3), (4, 3), (5, 2)]


def remplissage_grille_2(A, X, Y, n):
    """ A est la grille à remplir, X est la première case, Y la deuxième case, n le chiffre rempli dans la grille """
    x1 = X[0]
    y1 = X[1]

    x2 = Y[0]
    y2 = Y[1]

    while x1 != x2 and y1 != y2:
        x1 = X[0]
        y1 = X[1]
        x2 = Y[0]
        y2 = Y[1]

    if x1 == x2:
        y1, y2 = sorted((y1, y2))
        A[x1, y1:y2 + 1] = n

    if y1 == y2:
        x1, x2 = sorted((x1, x2))
        A[x1:x2 + 1, y1] = n


# Test
if __name__ == '__main__':
    print "Test de remplissage_grille_2:"

    L = (2, 2)
    A = np.zeros(L)
    B = np.zeros(L)
    X = [0, 0]
    Y = [0, 1]
    remplissage_grille_2(A, X, Y, 1)
    if A[0, 0] == 1 and A[0, 1] == 1 and A[1, 0] == 0 and A[1, 1] == 0:
        test = "OK"
    else:
        test = "NOK"
    print "remplissage_grille_2({})={} ; Résultat attendu ={}".format((B, X, Y, 1), A, np.array([[1, 1], [0, 0]]))
    print " ---> test {}".format(test)

    A = np.zeros(L)
    X = [1, 0]
    Y = [0, 0]
    remplissage_grille_2(A, X, Y, 2)
    if A[0, 0] == 2 and A[0, 1] == 0 and A[1, 0] == 2 and A[1, 1] == 0:
        test = "OK"
    else:
        test = "NOK"
    print "remplissage_grille_2({})={} ; Résultat attendu ={}".format((B, X, Y, 1), A, np.array([[2, 0], [2, 0]]))
    print " ---> test {}".format(test)


def compter_cases_vides(A, x, y):
    """ On compte le nombre de cases vides dans chaque direction """
    i = 1

    kd = 0  # à droite
    kb = 0  # en bas
    kg = 0  # à gauche
    kh = 0  # en haut

    while y + i < 10:
        if A[x, y + i] == 0:
            kd += 1
            i += 1
        else:
            i = 11

    i = 1
    while y - i >= 0:
        if A[x, y - i] == 0:
            kg = kg + 1
            i += 1
        else:
            i = 10

    i = 1
    while x - i >= 0:
        if A[x - i, y] == 0:
            kh = kh + 1
            i += 1
        else:
            i = 10

    i = 1
    while x + i < 10:
        if A[x + i, y] == 0:
            kb = kb + 1
            i += 1
        else:
            i = 11

    cases_vides = [(kd, 1), (kg, 3), (kh, 4), (kb, 2)]
    return cases_vides  # 1 désigne la direction "vers la droite", 2 "vers le bas", 3 "vers la gauche" et 4 "vers le haut"


# Test
if __name__ == '__main__':
    print "Test de compter_cases_vides :"

    L = (10, 10)
    A = np.zeros(L)
    X = [1, 1]
    Y = [1, 2]
    remplissage_grille_2(A, X, Y, 1)
    res = compter_cases_vides(A, 2, 1)
    if res[0][0] == 8 and res[1][0] == 1 and res[2][0] == 0 and res[3][0] == 7:
        test = "OK"
    else:
        test = "NOK"
    print " compter_cases_vides({})={} ; Resultat attendu ={}".format((A, 2, 1), res, [(8, 1), (1, 3), (0, 4), (7, 2)])
    print " ---> test {}".format(test)


def direction(cases_vides, taille_bateau):
    """ On choisit une direction parmis celles qui sont possibles """
    from random import choice
    possibilites = []
    for k in range(4):
        if cases_vides[k][0] >= (taille_bateau - 1):
            possibilites.append(cases_vides[k][1])
    direction = choice(possibilites)
    return direction


def deuxieme_coordonnee(X, k, p):
    """ X correspond à la première case où se situe le bateau, k correspond à la direction du bateau, p à la taille du
    bateau """
    X = copy.deepcopy(X)
    if k == 1:
        X[1] += p - 1
    if k == 3:
        X[1] -= p - 1
    if k == 2:
        X[0] += p - 1
    if k == 4:
        X[0] -= p - 1
    return X  # Le X retourné correspond à la dernière case du bateau


# Test
if __name__ == '__main__':
    print "Test de deuxieme_coordonnee :"
    X = [1, 1]
    res = deuxieme_coordonnee(X, 1, 3)
    if res == [1, 3]:
        test = "OK"
    else:
        test = "NOK"
    print " deuxieme_coordonnee({})={} ; Resultat attendu ={}".format((X, 1, 3), res, [1, 3])
    print " ---> test {}".format(test)
    X = [5, 8]
    res = deuxieme_coordonnee(X, 4, 4)
    if res == [2, 8]:
        test = "OK"
    else:
        test = "NOK"
    print " deuxieme_coordonnee({})={} ; Resultat attendu ={}".format((X, 4, 4), res, [2, 8])
    print " ---> test {}".format(test)


def placer_bateaux_ordi(A):
    """ On place les bateaux de façon aléatoire """
    for i in range(5):
        k = []
        while k == []:
            bateau_o1 = [randint(0, 9), randint(0, 9)]  # On choisit une première case au hasard
            x = bateau_o1[0]
            y = bateau_o1[1]

            while A[x, y] != 0:  # On vérifie que la case choisie est vide, sinon on choisit une autre case
                bateau_o1 = [randint(0, 9), randint(0, 9)]
                x = bateau_o1[0]
                y = bateau_o1[1]

            cases_vides = compter_cases_vides(A, x, y)
            taille = bateaux[i][1]
            k = direction(cases_vides, taille)

        bateau_o2 = deuxieme_coordonnee(
            bateau_o1, k, taille)  # On génère aléatoirement la dernière case du bateau de façon à ce qu'elle convienne

        n = bateaux[i][0]  # n est le numéro associé au bateau
        remplissage_grille_2(A, bateau_o1, bateau_o2, n)  # on place le bateau dans la grille

    return A
